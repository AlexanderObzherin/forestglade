// Fill out your copyright notice in the Description page of Project Settings.

#include "Ladybug.h"

#include "Runtime/Engine/Public/TimerManager.h"

// Sets default values
ALadybug::ALadybug()
	:
	mRunningTime( 0.0f ),
	mVelocityDirection( FVector::RightVector ),
	mVelocityFactor( 1.0f ),
	mYaw( 0.0f ),
	mAngularVelocity( 1.0f ),
	mHitCounter( 0 ),
	mMaxHit( 1 ),
	mWingsMoving( false ),
	TimerDelay( 4.0f )
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALadybug::BeginPlay()
{
	Super::BeginPlay();
	
	mWingsMoving = false;
	mVelocityDirection = mVelocityDirection.RotateAngleAxis( FMath::RandRange( 0, 360 ), this->GetActorUpVector() );
}

// Called every frame
void ALadybug::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Check for overlapping with other actors
	TArray<AActor*> OverlappingActors;
	this->GetOverlappingActors( OverlappingActors );

	if (OverlappingActors.Num() > (UINT)0)
	{
		if (mHitCounter < mMaxHit)
		{
			OnHit();
			++mHitCounter;
		}
		else
		{
			mWingsMoving = true;
			mHitCounter = 0;
		}
	}

	if (!mWingsMoving)
	{
		//we need to create 2d vectors of plane XY which means current direction and target direction
		//we can use cross product of these two vectors to detect where we should turn: left or right
		FVector2D curDir = FVector2D( GetActorRotation().Vector() );
		FVector2D targetDir = FVector2D( mVelocityDirection );

		float cross = FVector2D::CrossProduct( curDir, targetDir );

		//Compare current velocity direction with current object rotation
		if ((int)GetActorRotation().Yaw == (int)mVelocityDirection.Rotation().Yaw)
		{
			MoveForeward();
		}
		else
		{
			mAngularVelocity = (cross > 0 ? 1 : -1);
			mYaw += mAngularVelocity;
		}
		SetActorRotation( FRotator( 0.0f, mYaw, 0.0f ), ETeleportType::TeleportPhysics );
	}
	else
	{
		//time delay here to stop model and switch on WingsMoving animation
		if (!GetWorldTimerManager().IsTimerActive( mWingsMovingTimerHandle ))
		{
			GetWorldTimerManager().SetTimer( mWingsMovingTimerHandle, this, &ALadybug::CountdownHasFinished, TimerDelay, false );
		}
	}
}

void ALadybug::MoveForeward()
{
	SetActorLocation( GetActorLocation() + mVelocityDirection * mVelocityFactor, false, NULL, ETeleportType::TeleportPhysics );
}

void ALadybug::CountdownHasFinished()
{
	mWingsMoving = false;
	GetWorldTimerManager().ClearTimer( mWingsMovingTimerHandle );
}

void ALadybug::OnHit()
{
	//in this method we redirect actor

	//correct position after overlapping. It's needed to prevent stucking in collidable objects
	SetActorLocation( GetActorLocation() - mVelocityDirection * 10.0f, false, NULL, ETeleportType::TeleportPhysics );

	//Rotate velocity direction backward first
	mVelocityDirection = mVelocityDirection.MirrorByVector( mVelocityDirection );

	//then rotate in range min max
	mVelocityDirection = mVelocityDirection.RotateAngleAxis( FMath::RandRange( -60, 60 ), this->GetActorUpVector() );
}

