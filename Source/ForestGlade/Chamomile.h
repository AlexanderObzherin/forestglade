// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Chamomile.generated.h"

UCLASS()
class FORESTGLADE_API AChamomile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChamomile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float mRunningTime;

	UPROPERTY( EditAnywhere )
	float mMaxAmplitude;

	UPROPERTY( EditAnywhere )
	float mFactor;

	float mPitch;
	
};
