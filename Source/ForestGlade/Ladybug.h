// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ladybug.generated.h"

UCLASS()
class FORESTGLADE_API ALadybug : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALadybug();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Called to move foreward
	void MoveForeward();

	//variables for time delay
	FTimerHandle mWingsMovingTimerHandle;
	UPROPERTY()
	float TimerDelay;

	//this bool variable is using to set suitable animation sequence
	UPROPERTY( EditAnywhere, BlueprintReadOnly )
	bool mWingsMoving;

	//Called then counter is expired
	UFUNCTION()
	void CountdownHasFinished();


private:
	//Kinematics
	float mRunningTime;
	FVector mVelocityDirection;

	UPROPERTY( EditAnywhere )
	float mVelocityFactor;

	float mYaw;
	UPROPERTY( EditAnywhere )
	float mAngularVelocity;

	//variable to count number of hits
	unsigned int mHitCounter;

	//value of max times of hits
	UPROPERTY( EditAnywhere )
	unsigned int mMaxHit;

	//Called then ladybug on hit any other actors
	UFUNCTION()
	void OnHit();
		
};
