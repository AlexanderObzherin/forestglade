// Fill out your copyright notice in the Description page of Project Settings.

#include "Chamomile.h"


// Sets default values
AChamomile::AChamomile()
	:
	mRunningTime( 0.0f ),
	mMaxAmplitude( 10.0f ),
	mFactor( 1.0f ),
	mPitch( 0.0f )
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AChamomile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AChamomile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float deltaAngle = ( FMath::Sin( (mRunningTime + DeltaTime) * mFactor ) - FMath::Sin( mRunningTime * mFactor ) );
	mPitch += deltaAngle * mMaxAmplitude;
	mRunningTime += DeltaTime;
	SetActorRotation( FRotator( mPitch, 90.0f, 0.0f ) );
}

